from pynput import keyboard
from key_event_factory import KeyEventFactory
control=keyboard.Controller()

kf=KeyEventFactory()

def eventFilter(msg,data):
    print(msg,data)

# Collect events until released
with keyboard.Listener(win32_event_filter=eventFilter) as listener:
    listener.join()